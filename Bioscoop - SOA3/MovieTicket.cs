﻿namespace Bioscoop___SOA3
{
    public class MovieTicket
    {
        public int RowNr { get; private set; }
        public int SeatNr { get; private set; }
        public bool IsPremium { get; private set; }
        public MovieScreening MovieScreening { get; private set; }

        public MovieTicket(MovieScreening movieScreening, bool isPremiumReservation, int seatRow, int seatNr)
        {
            MovieScreening = movieScreening;
            IsPremium = isPremiumReservation;
            RowNr = seatRow;
            SeatNr = seatNr;
        }

        public double GetPrice()
        {
            if (IsPremium)
            {
                return MovieScreening.PricePerSeat + 3; // When this is a student -1
            } else
            {
                return MovieScreening.PricePerSeat;
            }
        }
    }
}
