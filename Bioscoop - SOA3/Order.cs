﻿using Bioscoop___SOA3.Implementations;
using Bioscoop___SOA3.Interfaces;
using System.Text.Json;

namespace Bioscoop___SOA3
{
    public class Order
    {
        public int OrderNr { get; private set; }
        public bool IsStudentOrder { get; private set; }
        public List<MovieTicket> MovieTickets { get; }
        public OrderState OrderState { get; set; }

        public Order(int orderNr, bool isStudentOrder, IObserver observer)
        {
            OrderNr = orderNr;
            IsStudentOrder = isStudentOrder;
            MovieTickets = new List<MovieTicket>();
            OrderState = new CreatedOrderState(this, observer);
        }

        public double CalculatePrice()
        {
            double totalPrice = 0;
            for(int i = 1; i <= MovieTickets.Count; i++)
            {
                
                if (i % 2 == 0 && (IsStudentOrder || !IsStudentOrder && new List<int> { 1, 2, 3, 4 }.Contains((int)MovieTickets[i - 1].MovieScreening.DateAndTime.DayOfWeek))) // When student order, second is always free
                {
                    // Elk 2e ticket is gratis voor studenten (elke dag van de week) of als het een voorstelling betreft op een
                    // doordeweekse dag (ma/di/wo/do) voor iedereen.
                    continue;
                }

                if (IsStudentOrder && MovieTickets[i - 1].IsPremium)
                {
                    totalPrice += MovieTickets[i-1].GetPrice() - 1;
                } else
                {
                    totalPrice += MovieTickets[i-1].GetPrice();
                }
            }

            if (!IsStudentOrder && new List<int> { 5, 6, 0 }.Contains((int)MovieTickets[0].MovieScreening.DateAndTime.DayOfWeek) && MovieTickets.Count > 5)
            {
                // In het weekend betaal je als niet - student de volle prijs, tenzij de bestelling uit 6 kaartjes of meer bestaat,
                // dan krijg je 10 % groepskorting.
                totalPrice *=  .9;
            }

            return Math.Round(totalPrice, 2);
        }

        public void AddSeatReservation(MovieTicket ticket)
        {
            MovieTickets.Add(ticket);
        }

        public void Export(TicketExportFormat exportFormat)
        {
            StreamWriter file = File.CreateText(@"D:\path.txt");

            switch (exportFormat)
            {
                case TicketExportFormat.JSON :
                    file.WriteLine(JsonSerializer.Serialize(MovieTickets));
                    break;
                case TicketExportFormat.PLAINTEXT :
                    foreach (MovieTicket MovieTicket in MovieTickets)
                    {
                        file.WriteLine(MovieTicket.ToString());
                    }
                    break;
                default :
                    file.Close();
                    break;
            }
        }

        public void SubmitOrder()
        {
            OrderState.SubmitOrder();
        }

        public void PayOrder()
        {
            OrderState.PayOrder();
        }

        public void CancelOrder()
        {
            OrderState.CancelOrder();
        }

        public void SendReminder()
        {
            OrderState.SendReminder();
        }
    }
}
