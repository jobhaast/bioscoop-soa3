using Bioscoop___SOA3.Interfaces;

namespace Bioscoop___SOA3.Implementations;

public class CreatedOrderState : OrderState
{
    public CreatedOrderState(Order order, IObserver observer) : base(order, observer)
    {
    }

    public override void SubmitOrder()
    {
        // State wordt nu reserved.
        Console.WriteLine("State has changed from created to reserved.");
        Order.OrderState = new ReservedOrderState(Order, Observer);
        Observer.Update();
    }
}