using Bioscoop___SOA3.Interfaces;

namespace Bioscoop___SOA3.Implementations;

// State blijft nu altijd Completed. Casus houdt geen rekening met annulering na betaling.
public class CompletedOrderState : OrderState
{
    public CompletedOrderState(Order order, IObserver observer) : base(order, observer)
    {
    }
}