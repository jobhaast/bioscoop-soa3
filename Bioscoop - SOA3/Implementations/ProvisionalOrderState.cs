using Bioscoop___SOA3.Interfaces;

namespace Bioscoop___SOA3.Implementations;

public class ProvisionalOrderState : OrderState
{
    public ProvisionalOrderState(Order order, IObserver observer) : base(order, observer)
    {
    }

    public override void PayOrder()
    {
        // State wordt Completed.
        Console.WriteLine("Order has been paid successfully.");
        Order.OrderState = new CompletedOrderState(Order, Observer);
        Observer.Update();
    }

    public override void CancelOrder()
    {
        // State wordt Cancelled.
        Console.WriteLine("Order has been cancelled successfully.");
        Order.OrderState = new CancelledOrderState(Order, Observer);
        Observer.Update();
    }
}