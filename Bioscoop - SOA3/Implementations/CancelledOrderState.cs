using Bioscoop___SOA3.Interfaces;

namespace Bioscoop___SOA3.Implementations;

// Order is geannulleerd. De state kan niet meer veranderen en blijft op Cancelled.
public class CancelledOrderState : OrderState
{
    public CancelledOrderState(Order order, IObserver observer) : base(order, observer)
    {
    }
}