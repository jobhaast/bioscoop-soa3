using Bioscoop___SOA3.Interfaces;

namespace Bioscoop___SOA3.Implementations;

public class ReservedOrderState : OrderState
{
    public ReservedOrderState(Order order, IObserver observer) : base(order, observer)
    {
    }

    public override void PayOrder()
    {
        // State verandert naar Completed.
        Console.WriteLine("Order has been paid successfully.");
        Order.OrderState = new CompletedOrderState(Order, Observer);
        Observer.Update();
    }

    public override void CancelOrder()
    {
        // State verandert naar Cancelled.
        Console.WriteLine();
        Order.OrderState = new CancelledOrderState(Order, Observer);
        Observer.Update();
    }

    public override void SendReminder()
    {
        // Reminder wordt 24 uur voor aanvang voor het eerst verstuurd. State wordt Provisional.
        Order.OrderState = new ProvisionalOrderState(Order, Observer);
        Observer.Update();
        // 12 uur voor aanvang wordt de state Cancelled.
    }
}