﻿namespace Bioscoop___SOA3
{
    public class MovieScreening
    {
        public DateTime DateAndTime { get; private set; }
        public double PricePerSeat { get; private set; }
        public Movie Movie { get; private set; }
        public List<MovieTicket> MovieTickets { get; private set; }

        public MovieScreening(Movie movie, DateTime localDateTime, double pricePerSeat)
        {
            Movie = movie;
            DateAndTime = localDateTime;
            PricePerSeat = pricePerSeat;
            MovieTickets = new List<MovieTicket>();
        }
    }
}
