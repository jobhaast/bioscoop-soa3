﻿namespace Bioscoop___SOA3
{
    public class Movie
    {
        public string Title { get; private set; }
        public List<MovieScreening> MovieScreenings { get; private set; }

        public Movie(string title)
        {
            Title = title;
            MovieScreenings = new List<MovieScreening>();
        }

        public void AddScreening(MovieScreening MovieScreening)
        {
            MovieScreenings.Add(MovieScreening);
        }
    }
}