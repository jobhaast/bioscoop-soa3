﻿namespace Bioscoop___SOA3.Interfaces
{
    public interface IObservable
    {
        public void NotifyObservers();
        public void SetObserver(IObserver observer);
    }
}
