namespace Bioscoop___SOA3.Interfaces;

public abstract class OrderState : IObservable
{
    public Order Order { get; set; }
    public IObserver Observer { get; set; }

    public OrderState(Order order, IObserver observer)
    {
        Order = order;
        Observer = observer;
    }

    public virtual void SubmitOrder()
    {
    }
    public virtual void PayOrder()
    {
    }
    public virtual void CancelOrder()
    {
    }
    public virtual void SendReminder()
    {
    }

    public void NotifyObservers()
    {
        Observer.Update();
    }

    public void SetObserver(IObserver observer)
    {
        Observer = observer;
    }
}