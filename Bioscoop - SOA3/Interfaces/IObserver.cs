﻿namespace Bioscoop___SOA3.Interfaces
{
    public interface IObserver
    {
        public void Update();
    }
}
