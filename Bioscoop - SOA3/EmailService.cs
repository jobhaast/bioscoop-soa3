﻿using Bioscoop___SOA3.Interfaces;

namespace Bioscoop___SOA3
{
    public class EmailService : IObserver
    {
        public void Update()
        {
            Console.WriteLine("Email sent");
        }
    }
}
