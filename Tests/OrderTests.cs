using Bioscoop___SOA3;
using Bioscoop___SOA3.Implementations;
using System;
using Xunit;

namespace Tests
{
    public class OrderTests
    {
        [Fact]
        public void CalculatePrice_WithSixStudentTickets_ShouldHave3FreeTickets()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            var movie = new Movie("Return of Jasper");
            var screening = new MovieScreening(movie, new DateTime(2022, 2, 6), 11);
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 1));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 2));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 3));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 4));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 5));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 6));

            // Act
            var price = order.CalculatePrice();

            // Assert
            Assert.Equal(33, price);
        }

        [Fact]
        public void CalculatePrice_WithFiveStudentTickets_ShouldHave2FreeTickets()
        {
            // Arrange
            var order = new Order(1, false, new EmailService());
            var movie = new Movie("Return of Jasper");
            var screening = new MovieScreening(movie, new DateTime(2022, 2, 7), 11);
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 1));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 2));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 3));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 4));
            order.AddSeatReservation(new MovieTicket(screening, false, 1, 5));

            // Act
            var price = order.CalculatePrice();

            // Assert
            Assert.Equal(33, price);
        }

        [Fact]
        public void CalculatePrice_WithSixPremiumTickets_ShouldGiveDiscount()
        {
            // Arrange
            var order = new Order(1, false, new EmailService());
            var movie = new Movie("Return of Jasper");
            var screening = new MovieScreening(movie, new DateTime(2022, 2, 6), 11);
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 1));
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 2));
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 3));
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 4));
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 5));
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 6));

            // Act
            var price = order.CalculatePrice();

            // Assert
            Assert.Equal(75.60, price);
        }

        [Fact]
        public void CalculatePrice_WithPremiumStudentTicket_ShouldGiveStudentPremiumPrice()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            var movie = new Movie("Return of Jasper");
            var screening = new MovieScreening(movie, new DateTime(2022, 2, 6), 11);
            order.AddSeatReservation(new MovieTicket(screening, true, 1, 1));

            // Act
            var price = order.CalculatePrice();

            // Assert
            Assert.Equal(13, price);
        }

        [Fact]
        public void SubmitOrder_WithOrderInCreatedState_ShouldSetStateToReservedState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());

            // Act
            order.SubmitOrder();

            // Assert
            Assert.Equal(typeof(ReservedOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void PayOrder_WithOrderInReservedState_ShouldSetStateToCompletedState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            order.OrderState = new ReservedOrderState(order, new EmailService());

            // Act
            order.PayOrder();

            // Assert
            Assert.Equal(typeof(CompletedOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void SendReminder_WithOrderInReservedState_ShouldSetStateToProvisionalState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            order.OrderState = new ReservedOrderState(order, new EmailService());

            // Act
            order.SendReminder();

            // Assert
            Assert.Equal(typeof(ProvisionalOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void CancelOrder_WithOrderInReservedState_ShouldSetStateToCancelledState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            order.OrderState = new ReservedOrderState(order, new EmailService());

            // Act
            order.CancelOrder();

            // Assert
            Assert.Equal(typeof(CancelledOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void PayOrder_WithOrderInProvisionalState_ShouldSetStateToCompletedState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            order.OrderState = new ProvisionalOrderState(order, new EmailService());

            // Act
            order.PayOrder();

            // Assert
            Assert.Equal(typeof(CompletedOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void CancelOrder_WithOrderInProvisionalState_ShouldSetStateToCanceledState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            order.OrderState = new ProvisionalOrderState(order, new EmailService());

            // Act
            order.CancelOrder();

            // Assert
            Assert.Equal(typeof(CancelledOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void PayOrder_WithOrderInCreatedState_ShouldMaintainCreatedState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());

            // Act
            order.PayOrder();

            // Assert
            Assert.Equal(typeof(CreatedOrderState), order.OrderState.GetType());
        }

        [Fact]
        public void PayOrder_WithOrderInCancelledState_ShouldMaintainCancelledState()
        {
            // Arrange
            var order = new Order(1, true, new EmailService());
            order.OrderState = new CancelledOrderState(order, new EmailService());

            // Act
            order.PayOrder();

            // Assert
            Assert.Equal(typeof(CancelledOrderState), order.OrderState.GetType());
        }
    }
}